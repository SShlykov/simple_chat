defmodule ChatLvWeb.PageController do
  use ChatLvWeb, :controller

  def index(conn, _params) do
    nodes = Node.list([:this, :visible]) |> Enum.reduce(nil, fn x, acc -> if is_nil(acc), do: "#{x}", else: "#{acc}, #{x}" end)
    node = node()
    render conn, "index.html", %{nodes: nodes, node: node}
  end
end
