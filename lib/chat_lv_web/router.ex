defmodule ChatLvWeb.Router do
  use ChatLvWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ChatLvWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", ChatLvWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

end
